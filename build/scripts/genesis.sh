#!/bin/sh

set -o pipefail

. "$(dirname "$0")/core.sh"

if [ "$NET" = "mocknet" ] || [ "$NET" = "testnet" ] || [ "$NET" = "stagenet" ]; then
  echo "Loading unsafe init for mocknet and testnet..."
  . "$(dirname "$0")/core-unsafe.sh"
  . "$(dirname "$0")/testnet/state.sh"
fi

NODES="${NODES:=1}"
SEED="${SEED:=ddnode}" # the hostname of the master node
ETH_HOST="${ETH_HOST:=http://ethereum:8545}"
THOR_BLOCK_TIME="${THOR_BLOCK_TIME:=5s}"
CHAIN_ID=${CHAIN_ID:=ddchain}

# this is required as it need to run ddnode init, otherwise tendermint related command doesn't work
if [ "$SEED" = "$(hostname)" ]; then
  if [ ! -f ~/.ddnode/config/priv_validator_key.json ]; then
    init_chain
    # remove the original generate genesis file, as below will init chain again
    rm -rf ~/.ddnode/config/genesis.json
  fi
fi

create_thor_user "$SIGNER_NAME" "$SIGNER_PASSWD" "$SIGNER_SEED_PHRASE"

VALIDATOR=$(ddnode tendermint show-validator | ddnode pubkey --bech cons)
NODE_ADDRESS=$(echo "$SIGNER_PASSWD" | ddnode keys show "$SIGNER_NAME" -a --keyring-backend file)
NODE_PUB_KEY=$(echo "$SIGNER_PASSWD" | ddnode keys show "$SIGNER_NAME" -p --keyring-backend file | ddnode pubkey)
VERSION=$(fetch_version)

if [ "$SEED" = "$(hostname)" ]; then
  echo "Setting MAYANode as genesis"
  if [ ! -f ~/.ddnode/config/genesis.json ]; then
    NODE_IP_ADDRESS=${EXTERNAL_IP:=$(curl -s http://whatismyip.akamai.com)}

    init_chain "$NODE_ADDRESS"
    add_node_account "$NODE_ADDRESS" "$VALIDATOR" "$NODE_PUB_KEY" "$VERSION" "$NODE_ADDRESS" "$NODE_PUB_KEY_ED25519" "$NODE_IP_ADDRESS"

    # disable default bank transfer, and opt to use our own custom one
    disable_bank_send
    disable_mint

    # for mocknet, add initial balances
  
    echo "Adding pool and lp bonder for first node"
    add_pool "BTC.BTC" 8
    # 100K karma of bond
    add_account "$NODE_ADDRESS" karma 1000000000
    add_account "$NODE_ADDRESS" maya 100000000000000
    add_lp "BTC.BTC" "$NODE_ADDRESS" "" 10000000000000 "$NODE_ADDRESS"

    # local cluster accounts for genesis nodes
    CAT=dd1uuds8pd92qnnq0udw0rpg0szpgcslc9pclc7xx
    add_account "$CAT" karma 300000000 # cat
    add_lp "BTC.BTC" "$CAT" "" 10000000000000 "$NODE_ADDRESS"
    DOG=dd1zf3gsk7edzwl9syyefvfhle37cjtql35g639t0
    add_account "$DOG" karma 100000000000000 # dog
    add_lp "BTC.BTC" "$DOG" "" 10000000000000 "$NODE_ADDRESS"
    FOX=dd13wrmhnh2qe98rjse30pl7u6jxszjjwl4k6rx8h
    add_account "$FOX" karma 3000000000 # fox
    add_lp "BTC.BTC" "$FOX" "" 10000000000000 "$NODE_ADDRESS"
    PIG=dd1qk8c8sfrmfm0tkncs0zxeutc8v5mx3pjd0e52g
    add_account "$PIG" karma 3000000000 # pig
    add_lp "BTC.BTC" "$PIG" "" 10000000000000 "$NODE_ADDRESS"

    # add_mayaname itzamna 5256000  dd160a6nf8ganf27g87g6cf9cyu59wxs6qtyp6sex MAYA
    # add_mayaname aaluxx 5256000 tmaya1thtn0cdcu2sujtklknlc9wl2dg09a07v0lgnkr tmaya1thtn0cdcu2sujtklknlc9wl2dg09a07v0lgnkr MAYA
    # add_mayaname wr 5256000 tmaya14lkndecaw0zkzu0yq4a0qq869hrs8hh7cqajxy 0x259fe33B59ae7C24B34C0cb957271Ae0f7786878 ETH
    # add_mayaname wr 5256000 tmaya14lkndecaw0zkzu0yq4a0qq869hrs8hh7cqajxy tmaya1a427q3v96psuj4fnughdw8glt5r7j38lk7v2wj MAYA
    # add_mayaname wr 5256000 tmaya14lkndecaw0zkzu0yq4a0qq869hrs8hh7cqajxy tthor1a427q3v96psuj4fnughdw8glt5r7j38lkfjxcz THOR

    echo "Setting up accounts"
    # heimdall accounts
    add_account dd18h0r0svrf40jwd7rsacv5mptw7vr8vcelynxje karma 5000000000000
    add_account dd1x0kkg5fev5t8p8aswmfuf07nm6tj6nn050w2cn karma 25000000000100
    add_account dd1eq2mlsz0d7s69ayx3vu8gsp4manx8e3s0r9v9v karma 25000000000100
    add_account dd1eq2mlsz0d7s69ayx3vu8gsp4manx8e3s0r9v9v karma 5090000000000

    reserve 22000000000000000

    # deploy eth contract
    deploy_eth_contract $ETH_HOST

    # override block time for faster smoke tests
    block_time "$THOR_BLOCK_TIME"

    echo "Genesis content"
    cat ~/.ddnode/config/genesis.json
    ddnode validate-genesis --trace
  fi
fi

# setup peer connection, typically only used for some mocknet configurations
if [ "$SEED" != "$(hostname)" ]; then
  if [ ! -f ~/.ddnode/config/genesis.json ]; then
    echo "Setting MAYANode as peer not genesis"

    init_chain "$NODE_ADDRESS"
    fetch_genesis "$SEED"
    NODE_ID=$(fetch_node_id "$SEED")
    echo "NODE ID: $NODE_ID"
    export THOR_TENDERMINT_P2P_PERSISTENT_PEERS="$NODE_ID@$SEED:$PORT_P2P"

    cat ~/.ddnode/config/genesis.json
  fi
fi

# render tendermint and cosmos configuration files
ddnode render-config

export SIGNER_NAME
export SIGNER_PASSWD
exec ddnode start

#!/bin/sh

set -o pipefail

export SIGNER_NAME="${SIGNER_NAME:=ddchain}"
export SIGNER_PASSWD="${SIGNER_PASSWD:=password}"

. "$(dirname "$0")/core.sh"

# auto populate seeds for fullnode if unprovided
if [ -z "$SEEDS" ]; then
  if [ "$NET" = "mainnet" ]; then
    SEEDS=$(curl -s https://seed.thorchain.info/ | jq -r '. | join(",")')
  elif [ "$NET" = "testnet" ]; then
    SEEDS=$(curl -s https://testnet.seed.thorchain.info/ | jq -r '. | join(",")')
  elif [ "$NET" = "stagenet" ]; then
    SEEDS="stagenet.ddchain.info"
  fi
fi

if [ ! -f ~/.ddnode/config/genesis.json ]; then
  init_chain

  fetch_genesis_from_seeds "$SEEDS"
fi

# render tendermint and cosmos configuration files
ddnode render-config

exec ddnode start

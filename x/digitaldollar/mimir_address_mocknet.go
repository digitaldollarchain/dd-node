//go:build mocknet
// +build mocknet

package digitaldollar

// ADMINS hard coded admin address
var ADMINS = []string{
	"dd1dvxlqzk6p7fyjsd9eycqnfgntnx725qxccu3m7",

	// "dog" mnemonic key for local testing:
	// dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog fossil
	"dd1zf3gsk7edzwl9syyefvfhle37cjtql35g639t0",
}

package digitaldollar

import (
	"fmt"

	"gitlab.com/digitaldollarchain/dd-node/common/cosmos"
)

func (h DonateHandler) validateV80(ctx cosmos.Context, msg MsgDonate) error {
	if err := msg.ValidateBasic(); err != nil {
		return err
	}
	if msg.Asset.IsSyntheticAsset() {
		ctx.Logger().Error("asset cannot be synth", "error", errInvalidMessage)
		return errInvalidMessage
	}

	if isLiquidityAuction(ctx, h.mgr.Keeper()) {
		pool, err := h.mgr.Keeper().GetPool(ctx, msg.Asset)
		if err != nil {
			return ErrInternal(err, fmt.Sprintf("fail to get pool(%s)", msg.Asset))
		}

		if pool.Status != PoolAvailable {
			return errInvalidPoolStatus
		}
	}

	return nil
}

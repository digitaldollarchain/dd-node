package types

import (
	"github.com/cosmos/cosmos-sdk/codec"
	cdctypes "github.com/cosmos/cosmos-sdk/codec/types"

	"gitlab.com/digitaldollarchain/dd-node/common/cosmos"
)

var (
	amino     = codec.NewLegacyAmino()
	ModuleCdc = codec.NewAminoCodec(amino)
)

func init() {
	RegisterCodec(amino)
}

// RegisterCodec register the msg types for amino
func RegisterCodec(cdc *codec.LegacyAmino) {
	cdc.RegisterConcrete(&MsgSwap{}, "digitaldollar/Swap", nil)
	cdc.RegisterConcrete(&MsgTssPool{}, "digitaldollar/TssPool", nil)
	cdc.RegisterConcrete(&MsgTssKeysignFail{}, "digitaldollar/TssKeysignFail", nil)
	cdc.RegisterConcrete(&MsgAddLiquidity{}, "digitaldollar/AddLiquidity", nil)
	cdc.RegisterConcrete(&MsgWithdrawLiquidity{}, "digitaldollar/WidthdrawLiquidity", nil)
	cdc.RegisterConcrete(&MsgObservedTxIn{}, "digitaldollar/ObservedTxIn", nil)
	cdc.RegisterConcrete(&MsgObservedTxOut{}, "digitaldollar/ObservedTxOut", nil)
	cdc.RegisterConcrete(&MsgDonate{}, "digitaldollar/MsgDonate", nil)
	cdc.RegisterConcrete(&MsgBond{}, "digitaldollar/MsgBond", nil)
	cdc.RegisterConcrete(&MsgUnBond{}, "digitaldollar/MsgUnBond", nil)
	cdc.RegisterConcrete(&MsgLeave{}, "digitaldollar/MsgLeave", nil)
	cdc.RegisterConcrete(&MsgNoOp{}, "digitaldollar/MsgNoOp", nil)
	cdc.RegisterConcrete(&MsgOutboundTx{}, "digitaldollar/MsgOutboundTx", nil)
	cdc.RegisterConcrete(&MsgSetVersion{}, "digitaldollar/MsgSetVersion", nil)
	cdc.RegisterConcrete(&MsgSetNodeKeys{}, "digitaldollar/MsgSetNodeKeys", nil)
	cdc.RegisterConcrete(&MsgSetAztecAddress{}, "digitaldollar/MsgSetAztecAddress", nil)
	cdc.RegisterConcrete(&MsgSetIPAddress{}, "digitaldollar/MsgSetIPAddress", nil)
	cdc.RegisterConcrete(&MsgYggdrasil{}, "digitaldollar/MsgYggdrasil", nil)
	cdc.RegisterConcrete(&MsgReserveContributor{}, "digitaldollar/MsgReserveContributor", nil)
	cdc.RegisterConcrete(&MsgErrataTx{}, "digitaldollar/MsgErrataTx", nil)
	cdc.RegisterConcrete(&MsgBan{}, "digitaldollar/MsgBan", nil)
	cdc.RegisterConcrete(&MsgMimir{}, "digitaldollar/MsgMimir", nil)
	cdc.RegisterConcrete(&MsgDeposit{}, "digitaldollar/MsgDeposit", nil)
	cdc.RegisterConcrete(&MsgNetworkFee{}, "digitaldollar/MsgNetworkFee", nil)
	cdc.RegisterConcrete(&MsgMigrate{}, "digitaldollar/MsgMigrate", nil)
	cdc.RegisterConcrete(&MsgRagnarok{}, "digitaldollar/MsgRagnarok", nil)
	cdc.RegisterConcrete(&MsgRefundTx{}, "digitaldollar/MsgRefundTx", nil)
	cdc.RegisterConcrete(&MsgSend{}, "digitaldollar/MsgSend", nil)
	cdc.RegisterConcrete(&MsgNodePauseChain{}, "digitaldollar/MsgNodePauseChain", nil)
	cdc.RegisterConcrete(&MsgSolvency{}, "digitaldollar/MsgSolvency", nil)
	cdc.RegisterConcrete(&MsgManageMAYAName{}, "digitaldollar/MsgManageMAYAName", nil)
}

// RegisterInterfaces register the types
func RegisterInterfaces(registry cdctypes.InterfaceRegistry) {
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgSwap{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgTssPool{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgTssKeysignFail{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgAddLiquidity{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgWithdrawLiquidity{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgObservedTxIn{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgObservedTxOut{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgDonate{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgBond{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgUnBond{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgLeave{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgNoOp{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgOutboundTx{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgSetVersion{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgSetNodeKeys{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgSetAztecAddress{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgSetIPAddress{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgYggdrasil{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgReserveContributor{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgErrataTx{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgBan{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgMimir{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgDeposit{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgNetworkFee{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgMigrate{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgRagnarok{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgRefundTx{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgSend{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgNodePauseChain{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgManageMAYAName{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgSolvency{})
}

package types

import (
	cosmos "gitlab.com/digitaldollarchain/dd-node/common/cosmos"
)

// NewProtocolOwnedLiquidity create a new instance ProtocolOwnedLiquidity it is empty though
func NewProtocolOwnedLiquidity() ProtocolOwnedLiquidity {
	return ProtocolOwnedLiquidity{
		KarmaDeposited: cosmos.ZeroUint(),
		KarmaWithdrawn: cosmos.ZeroUint(),
	}
}

func (pol ProtocolOwnedLiquidity) CurrentDeposit() cosmos.Int {
	deposited := cosmos.NewIntFromBigInt(pol.KarmaDeposited.BigInt())
	withdrawn := cosmos.NewIntFromBigInt(pol.KarmaWithdrawn.BigInt())
	return deposited.Sub(withdrawn)
}

// PnL - Profit and Loss
func (pol ProtocolOwnedLiquidity) PnL(value cosmos.Uint) cosmos.Int {
	deposited := cosmos.NewIntFromBigInt(pol.KarmaDeposited.BigInt())
	withdrawn := cosmos.NewIntFromBigInt(pol.KarmaWithdrawn.BigInt())
	v := cosmos.NewIntFromBigInt(value.BigInt())
	return withdrawn.Sub(deposited).Add(v)
}

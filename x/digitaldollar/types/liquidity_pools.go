//go:build !mocknet && !stagenet
// +build !mocknet,!stagenet

package types

import (
	"gitlab.com/digitaldollarchain/dd-node/common"
)

var LiquidityPools = common.Assets{
	common.BTCAsset,
	common.BNBAsset,
	common.ETHAsset,
}

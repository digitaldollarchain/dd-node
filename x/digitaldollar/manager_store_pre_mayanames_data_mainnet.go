//go:build !testnet && !mocknet && !stagenet
// +build !testnet,!mocknet,!stagenet

package digitaldollar

import _ "embed"

//go:embed preregister_mayanames.json
var preregisterMAYANames []byte

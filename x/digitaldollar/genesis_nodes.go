//go:build !mocknet && !stagenet
// +build !mocknet,!stagenet

package digitaldollar

var GenesisNodes = []string{
	"maya1q3jj8n8pkvl2kjv3pajdyju4hp92cmxnadknd2",
	"maya10sy79jhw9hw9sqwdgu0k4mw4qawzl7czewzs47",
	"maya1gv85v0jvc0rsjunku3qxempax6kmrg5jqh8vmg",
	"maya1vm43yk3jq0evzn2u6a97mh2k9x4xf5mzp62g23",
	"maya1g8dzs4ywxhf8hynaddw4mhwzlwzjfccakkfch7",
}

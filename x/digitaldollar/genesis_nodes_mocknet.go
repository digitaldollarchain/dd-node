//go:build mocknet
// +build mocknet

package digitaldollar

var GenesisNodes = []string{
	"dd1uuds8pd92qnnq0udw0rpg0szpgcslc9pclc7xx",
	"dd1zf3gsk7edzwl9syyefvfhle37cjtql35g639t0",
	"dd13wrmhnh2qe98rjse30pl7u6jxszjjwl4k6rx8h",
	"dd1qk8c8sfrmfm0tkncs0zxeutc8v5mx3pjd0e52g",
}

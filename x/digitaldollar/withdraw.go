package digitaldollar

import (
	"github.com/blang/semver"

	"gitlab.com/digitaldollarchain/dd-node/common/cosmos"
)

func withdraw(ctx cosmos.Context, msg MsgWithdrawLiquidity, mgr Manager) (cosmos.Uint, cosmos.Uint, cosmos.Uint, cosmos.Uint, cosmos.Uint, error) {
	version := mgr.GetVersion()
	switch {
	case version.GTE(semver.MustParse("1.91.0")):
		return withdrawV91(ctx, msg, mgr)
	default:
		zero := cosmos.ZeroUint()
		return zero, zero, zero, zero, zero, errInvalidVersion
	}
}

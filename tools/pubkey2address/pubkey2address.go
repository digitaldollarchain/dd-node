package main

import (
	"flag"
	"fmt"

	"gitlab.com/digitaldollarchain/dd-node/common"
	"gitlab.com/digitaldollarchain/dd-node/common/cosmos"
)

func main() {
	raw := flag.String("p", "", "thor bech32 pubkey")
	flag.Parse()

	if len(*raw) == 0 {
		panic("no pubkey provided")
	}

	config := cosmos.GetConfig()
	config.SetBech32PrefixForAccount("dd", "ddpub")
	config.SetBech32PrefixForValidator("ddv", "ddvpub")
	config.SetBech32PrefixForConsensusNode("ddc", "ddcpub")
	config.Seal()
	pk, err := common.NewPubKey(*raw)
	if err != nil {
		panic(err)
	}

	chains := common.Chains{
		common.BASEChain,
		common.BNBChain,
		common.BTCChain,
		common.ETHChain,
		common.BASEChain,
	}

	for _, chain := range chains {
		addr, err := pk.GetAddress(chain)
		if err != nil {
			panic(err)
		}
		fmt.Printf("%s Address: %s\n", chain.String(), addr)
	}
}

//go:build !testnet && !mocknet && !stagenet
// +build !testnet,!mocknet,!stagenet

package cmd

const (
	Bech32PrefixAccAddr         = "dd"
	Bech32PrefixAccPub          = "ddpub"
	Bech32PrefixValAddr         = "ddv"
	Bech32PrefixValPub          = "ddvpub"
	Bech32PrefixConsAddr        = "ddc"
	Bech32PrefixConsPub         = "ddcpub"
	DenomRegex                  = `[a-zA-Z][a-zA-Z0-9:\\/\\\-\\_\\.]{2,127}`
	BASEChainCoinType    uint32 = 931
	BASEChainCoinPurpose uint32 = 44
	BASEChainHDPath      string = `m/44'/931'/0'/0/0`
)

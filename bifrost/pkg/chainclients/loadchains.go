package chainclients

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/thorchain/tss/go-tss/tss"

	"gitlab.com/digitaldollarchain/dd-node/bifrost/pkg/chainclients/avalanche"
	"gitlab.com/digitaldollarchain/dd-node/bifrost/pkg/chainclients/dogecoin"
	"gitlab.com/digitaldollarchain/dd-node/bifrost/pkg/chainclients/gaia"
	"gitlab.com/digitaldollarchain/dd-node/bifrost/pkg/chainclients/thorchain"

	"gitlab.com/digitaldollarchain/dd-node/bifrost/ddclient"
	"gitlab.com/digitaldollarchain/dd-node/bifrost/metrics"
	"gitlab.com/digitaldollarchain/dd-node/bifrost/pkg/chainclients/binance"
	"gitlab.com/digitaldollarchain/dd-node/bifrost/pkg/chainclients/bitcoin"
	"gitlab.com/digitaldollarchain/dd-node/bifrost/pkg/chainclients/bitcoincash"
	"gitlab.com/digitaldollarchain/dd-node/bifrost/pkg/chainclients/ethereum"
	"gitlab.com/digitaldollarchain/dd-node/bifrost/pkg/chainclients/litecoin"
	"gitlab.com/digitaldollarchain/dd-node/bifrost/pubkeymanager"
	"gitlab.com/digitaldollarchain/dd-node/common"
	"gitlab.com/digitaldollarchain/dd-node/config"
)

// LoadChains returns chain clients from chain configuration
func LoadChains(thorKeys *ddclient.Keys,
	cfg map[common.Chain]config.BifrostChainConfiguration,
	server *tss.TssServer,
	thorchainBridge *ddclient.DdchainBridge,
	m *metrics.Metrics,
	pubKeyValidator pubkeymanager.PubKeyValidator,
	poolMgr ddclient.PoolManager,
) map[common.Chain]ChainClient {
	logger := log.Logger.With().Str("module", "bifrost").Logger()
	chains := make(map[common.Chain]ChainClient)

	for _, chain := range cfg {
		if chain.Disabled {
			logger.Info().Msgf("%s chain is disabled by configure", chain.ChainID)
			continue
		}
		switch chain.ChainID {
		case common.BNBChain:
			bnb, err := binance.NewBinance(thorKeys, chain, server, thorchainBridge, m)
			if err != nil {
				logger.Fatal().Err(err).Str("chain_id", chain.ChainID.String()).Msg("fail to load chain")
				continue
			}
			chains[common.BNBChain] = bnb
		case common.ETHChain:
			eth, err := ethereum.NewClient(thorKeys, chain, server, thorchainBridge, m, pubKeyValidator, poolMgr)
			if err != nil {
				logger.Fatal().Err(err).Str("chain_id", chain.ChainID.String()).Msg("fail to load chain")
				continue
			}
			chains[common.ETHChain] = eth
		case common.BTCChain:
			btc, err := bitcoin.NewClient(thorKeys, chain, server, thorchainBridge, m)
			if err != nil {
				logger.Fatal().Err(err).Str("chain_id", chain.ChainID.String()).Msg("fail to load chain")
				continue
			}
			pubKeyValidator.RegisterCallback(btc.RegisterPublicKey)
			chains[common.BTCChain] = btc
		case common.BCHChain:
			bch, err := bitcoincash.NewClient(thorKeys, chain, server, thorchainBridge, m)
			if err != nil {
				logger.Fatal().Err(err).Str("chain_id", chain.ChainID.String()).Msg("fail to load chain")
				continue
			}
			pubKeyValidator.RegisterCallback(bch.RegisterPublicKey)
			chains[common.BCHChain] = bch
		case common.LTCChain:
			ltc, err := litecoin.NewClient(thorKeys, chain, server, thorchainBridge, m)
			if err != nil {
				logger.Fatal().Err(err).Str("chain_id", chain.ChainID.String()).Msg("fail to load chain")
				continue
			}
			pubKeyValidator.RegisterCallback(ltc.RegisterPublicKey)
			chains[common.LTCChain] = ltc
		case common.DOGEChain:
			doge, err := dogecoin.NewClient(thorKeys, chain, server, thorchainBridge, m)
			if err != nil {
				logger.Fatal().Err(err).Str("chain_id", chain.ChainID.String()).Msg("fail to load chain")
				continue
			}
			pubKeyValidator.RegisterCallback(doge.RegisterPublicKey)
			chains[common.DOGEChain] = doge
		case common.THORChain:
			logger.Debug().Msg("Loading THORCHAIN")
			thor, err := thorchain.NewCosmosClient(thorKeys, chain, server, thorchainBridge, m)
			if err != nil {
				logger.Fatal().Err(err).Str("chain_id", chain.ChainID.String()).Msg("fail to load chain")
				continue
			}
			chains[common.THORChain] = thor
		case common.AVAXChain:
			avax, err := avalanche.NewAvalancheClient(thorKeys, chain, server, thorchainBridge, m, pubKeyValidator, poolMgr)
			if err != nil {
				logger.Fatal().Err(err).Str("chain_id", chain.ChainID.String()).Msg("fail to load chain")
				continue
			}
			chains[common.AVAXChain] = avax
		case common.GAIAChain:
			gaia, err := gaia.NewCosmosClient(thorKeys, chain, server, thorchainBridge, m)
			if err != nil {
				logger.Fatal().Err(err).Str("chain_id", chain.ChainID.String()).Msg("fail to load chain")
				continue
			}
			chains[common.GAIAChain] = gaia
		}
	}

	return chains
}

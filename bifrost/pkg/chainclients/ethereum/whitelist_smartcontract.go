//go:build !testnet && !mocknet && !stagenet
// +build !testnet,!mocknet,!stagenet

package ethereum

import (
	"gitlab.com/digitaldollarchain/dd-node/common"
)

var whitelistSmartContractAddress = []common.Address{}

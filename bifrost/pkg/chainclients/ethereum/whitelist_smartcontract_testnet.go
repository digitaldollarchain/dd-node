//go:build testnet || mocknet
// +build testnet mocknet

package ethereum

import (
	"gitlab.com/digitaldollarchain/dd-node/common"
)

var whitelistSmartContractAddress = []common.Address{}

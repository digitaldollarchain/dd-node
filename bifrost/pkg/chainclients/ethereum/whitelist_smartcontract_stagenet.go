//go:build stagenet
// +build stagenet

package ethereum

import (
	"gitlab.com/digitaldollarchain/dd-node/common"
)

var whitelistSmartContractAddress = []common.Address{}

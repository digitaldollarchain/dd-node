package types

import (
	stypes "gitlab.com/digitaldollarchain/dd-node/x/digitaldollar/types"
)

type Msg struct {
	Type  string                 `json:"type"`
	Value stypes.MsgObservedTxIn `json:"value"`
}

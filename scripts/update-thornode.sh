#!/bin/bash
VERSION=$1

if ! hash git-imerge 2>/dev/null; then
  echo "git-imerge is not installed"
  echo "Check https://github.com/mhagger/git-imerge"
fi

# Check if local update-$VERSION branch already exists
if git show-ref --verify --quiet refs/heads/update-"$VERSION"; then
  git branch -D update-"$VERSION"
fi

# Check if there are uncommited changes
if ! git diff-index --quiet HEAD; then
  echo "You have uncommited changes, commit or stash them before updating"
  exit
fi

git remote add thorchain git@gitlab.com:thorchain/thornode.git
git fetch thorchain "$VERSION"
git remote rm thorchain
git-imerge start --name="update-$VERSION" --first-parent "$VERSION"

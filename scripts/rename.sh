#!/bin/bash

# Check if ripgrep and git are installed
if ! hash rg 2>/dev/null; then
  echo "ripgrep is not installed"
  echo "Check https://github.com/BurntSushi/ripgrep"
  exit
fi

rename_imports() {
  rg gitlab.com.thorchain/thornode -l | xargs sed -i 's@gitlab.com/digitaldollarchain/dd-node@gitlab.com/digitaldollarchain/dd-node@g'
}

rename_definitions() {
  rg THORChain -g '!scripts/*' -g '!*loadchains.go' -g '!common/chain.go' -l | xargs sed -i 's@THORChain@BASEChain@g'
  rg RuneNative -g '!scripts/*' -l | xargs sed -i 's@RuneNative@BaseNative@g'
  rg RuneAsset -g '!scripts/*' -l | xargs sed -i 's@RuneAsset@BaseAsset@g'
  rg GetRandomTHORAddress -l | xargs sed -i 's@GetRandomTHORAddress@GetRandomBaseAddress@g'
}

rename_tokens() {
  rg Rune67CAsset -l | xargs sed -i 's@Rune67CAsset@BNBAsset@g'
}

rename_addresses() {
  rg --no-filename -g '!bifrost/pk/chainclients/thorchain' -g '!test/smoke/chains/aliases.py' -g '!build/docker/mocknet/thorchain/genesis.sh' -NoP 'tthor[a-z0-9]{39}' |
    sort | uniq | \
    while read address
    do
      # Sed from input
      add=$(echo $address | sed 's/tthor/dd/') 
      out=$(thornode query bank balances $add 2>&1 | grep -Po 'expected [a-z0-9]{6} got [a-z0-9]{6}')
      exp=$(echo $out | grep -Po '(?<=expected ).*(?= got)')
      got=$(echo $out | grep -Po '(?<=got ).*')

      # check if either is empty and continue
      if [ -z "$exp" ] || [ -z "$got" ]; then
        echo "error: $address $add"
        continue
      fi

      newadd=$(echo $add | sed "s/$exp/$got/")
      echo "change $address -> $newadd"
      # trim of spaces the address
      address=$(echo $address | tr -d '[:space:]')
      grep --exclude=bifrost/pkg/chainclients/thorchain --exclude=test/smoke/chains/aliases.py --exclude=build/docker/mocknet/thorchain/genesis.sh $address -rl \
        | xargs sed -i "s/$address/$newadd/g"
    done
}

rename_imports
# rename_definitions
